#-------------------------------------------------
#
# Project created by QtCreator 2016-01-12T12:54:12
#
#-------------------------------------------------

QT       += core gui widgets multimedia multimediawidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QTableWidget
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    budilnik.cpp

HEADERS  += mainwindow.h \
    budilnik.h

FORMS    += mainwindow.ui \
    budilnik.ui

RESOURCES += \
    res/res.qrc
