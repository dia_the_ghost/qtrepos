#ifndef BUDILNIK_H
#define BUDILNIK_H

#include <QWidget>
#include <QTime>
#include <QTimer>
#include <QDateTimeEdit>
#include <QDateTime>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QSound>
#include <QSoundEffect>

namespace Ui {
class Budilnik;
}

class Budilnik : public QWidget
{
    Q_OBJECT

public:
    explicit Budilnik(QWidget *parent = 0);
    ~Budilnik();

private:
    Ui::Budilnik *ui;

    QDateTime alarmTime;
    QMediaPlayer *player;
    QMediaPlaylist *playlist;

    qint64 finalAlarmTime;
    int finalTime;

private slots:
    void alert();
    void setAlarmTime();
    void stopAlarm();
    void delayAlarm();
};

#endif // BUDILNIK_H
