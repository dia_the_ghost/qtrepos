#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTableWidget>
#include <QTime>
#include <QTimer>
#include <QSound>
#include <QSoundEffect>
#include <QMessageBox>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    QTimer *timer;
    int row;
    int column;

private slots:
    void timeUpdate();
    void on_actionVersion_triggered();
    void on_actionAuthors_triggered();
    void on_actionHelp_triggered();

    void on_actionNew_alarm_triggered();
};

#endif // MAINWINDOW_H
