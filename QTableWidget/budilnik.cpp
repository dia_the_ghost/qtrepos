#include "budilnik.h"
#include "ui_budilnik.h"

Budilnik::Budilnik(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Budilnik)
{
    ui->setupUi(this);

    player = new QMediaPlayer(this);
    playlist = new QMediaPlaylist(player);

    playlist->addMedia(QUrl("qrc:/n/soldier.mp3"));
    playlist->setPlaybackMode(QMediaPlaylist::CurrentItemOnce);

    player->setPlaylist(playlist);

    //засунем текущее время и дату в dateTimeEdit
    ui->dateTimeEdit->setDateTime(QDateTime::currentDateTime());

    connect(ui->buttonStart, SIGNAL(clicked()), this, SLOT(setAlarmTime()));
    connect(ui->buttonDelay, SIGNAL(clicked()), this, SLOT(delayAlarm()));
    connect(ui->buttonStop, SIGNAL(clicked()), this, SLOT(stopAlarm()));
}

Budilnik::~Budilnik()
{
    delete ui;
}

void Budilnik::alert()
{
    qDebug() << "WAKE UP";
    player->setVolume(50);
    player->play();
}

void Budilnik::setAlarmTime()
{
    //выставленное юзером время
    alarmTime = ui->dateTimeEdit->dateTime();

    //вычислим разницу
    finalAlarmTime = alarmTime.toMSecsSinceEpoch() - QDateTime::currentMSecsSinceEpoch();
    finalTime = static_cast<int>(finalAlarmTime);

    //передаем разницу в singleShot()
    QTimer::singleShot(finalTime, this, SLOT(alert()));
}

void Budilnik::stopAlarm()
{
    player->stop();
}

void Budilnik::delayAlarm()
{
    //stop the alarm
    stopAlarm();

    //set datetimeedit
    ui->dateTimeEdit->setDateTime(alarmTime.addMSecs(300000));

    //reset alarm time (setAlarmTime);
    setAlarmTime();
}
