#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "budilnik.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle("Nemesis");
    setWindowIcon(QIcon(":/n/nemesis.png"));

    timer = new QTimer();
    timer->setInterval(1000);

    row = 0;
    column = 0;

    ui->tableWidget->setMaximumSize(400, 400);
    ui->tableWidget->setRowCount(1);
    ui->tableWidget->setColumnCount(1);
    ui->tableWidget->setColumnWidth(column, 350);

    ui->tableWidget->setRowHeight(row, 50);

    ui->tableWidget->setHorizontalHeaderLabels(QStringList() << "Set the alarm time");

    Budilnik *bud = new Budilnik();
    ui->tableWidget->setCellWidget(row, column, bud);

    timer->start();
    connect(timer, SIGNAL(timeout()), this, SLOT(timeUpdate()));

    QSound::play(":/n/hi.wav");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::timeUpdate()
{
    ui->labelClock->setText(QTime::currentTime().toString());
    ui->labelClock->setAlignment(Qt::AlignCenter);
}


void MainWindow::on_actionVersion_triggered()
{
    QMessageBox msg;
    msg.setWindowTitle("O Nemesis");
    msg.setWindowIcon(QIcon(":/n/nemesis.png"));

    msg.setText("<h3>Nemesis 1.3.0 alpha (opensource).</h3> Основан на Qt 5.5.0 (MinGW 2015, 32 бита)");
    msg.exec();
}

void MainWindow::on_actionAuthors_triggered()
{
    QMessageBox msg;
    msg.setWindowTitle("Authors");
    msg.setWindowIcon(QIcon(":/n/nemesis.png"));

    msg.setText("<h4>Диана Волкова<br>Евгений Крупенков</h4>");
    msg.exec();
}

void MainWindow::on_actionHelp_triggered()
{
    QMessageBox msg;
    msg.setWindowTitle("Help");
    msg.setWindowIcon(QIcon(":/n/nemesis.png"));

    msg.setText("<h3>Справка.</h3> Для установки будильника выставите дату и время, затем нажмите start.<br>Будьте внимательны при установке времени.<br>Кнопка Delay откладывает время на 5 минут.");
    msg.exec();
}

void MainWindow::on_actionNew_alarm_triggered()
{
    //создаем новый объект будильника
    Budilnik *newBudilnik = new Budilnik();

    //создаем ячейку куда его пихать
    ++row;
    ui->tableWidget->insertRow(row);
    ui->tableWidget->setRowHeight(row, 50);

    //и всовываем его туда
    ui->tableWidget->setCellWidget(row, column, newBudilnik);
}

//void QTableWidget::insertRow ( int row ) [slot]
