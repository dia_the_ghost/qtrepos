import QtQuick 2.5

Rectangle {
    anchors.fill: parent
    color: "pink"

    Text {
        text: qsTr("Фрагмент 3")
        color: "black"
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.margins: 50
        font.pixelSize: 30

        renderType: Text.NativeRendering
    }
}
