import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Loader")

    RowLayout {
        id: rowLayout
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 5

        Button {
            text: qsTr("Фрагмент 1")
            onClicked: loader.source = "Fragment1.qml"
        }

        Button
        {
            text: qsTr("Фрагмент 2")
            onClicked: loader.setSource("Fragment2.qml", {"opacity": 1})
        }

        Button
        {
            text: qsTr("Фрагмент 3")
            onClicked: loader.setSource("Fragment3.qml", {"opacity": 0.5})
        }

        Button
        {
            text: qsTr("Фрагмент 4")
            onClicked: loader.sourceComponent = fragment4
        }

        Button
        {
            text: qsTr("Фрагмент 5")
            onClicked: loader.sourceComponent = fragment5
        }

    }

    Loader {
        id: loader
        anchors.top: rowLayout.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 5

        source: "Fragment1.qml"
    }

    Component {
        id: fragment4

        Rectangle{
            anchors.fill: parent
            color: "blue"

            Text {
                text: qsTr("Фрагмент 4")
                color: "white"

                anchors.top: parent.top
                anchors.right: parent.right
                anchors.margins: 50
                font.pixelSize: 30

                renderType: Text.NativeRendering
            }
        }
    }

    Component {
        id: fragment5

        Rectangle {
            anchors.fill: parent
            color: "black"

            Text {
                text: qsTr("Фрагмент 5")
                color: "white"
                anchors.top: parent.top
                anchors.right: parent.right
                anchors.margins: 50
                font.pixelSize: 30

                renderType: Text.NativeRendering
            }
        }
    }

}
