import QtQuick 2.5
import QtQuick.Controls 1.4

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    Rectangle{
        id: rect1

        color: "#00B000"
        width: 50
        height: 50
        radius: 5
        anchors.left: parent.left

        MouseArea{
            anchors.fill: parent
            acceptedButtons: Qt.LeftButton | Qt.RightButton | Qt.MiddleButton
            onClicked: {
                if(mouse.button === Qt.RightButton)
                    parent.color = 'back';
                else if(mouse.button === Qt.MiddleButton)
                    parent.color = '#00B000';
                else
                    parent.color = 'red';
            }
        }
    }

    Rectangle{
        id: rect2

        color: "steelblue"
        width: 50
        height: 50
        radius: 5
        //anchors.left: rect1.right
        //anchors.top: rect1.bottom
        //anchors.leftMargin: 13
        x: 60
        y: 60

        MouseArea{
            id: mouseArea
            anchors.fill: parent
        }
        states: State {
            name: "moved"
            when: mouseArea.pressed
            PropertyChanges {
                target: rect2
                x: 200
                y: 400
            }
        }

        transitions: Transition{
            NumberAnimation {
                properties: "x, y"
                easing.type: Easing.InOutQuad
                easing.amplitude: 2.0
                easing.period: 1.5
            }
        }
    }
}
