import QtQuick 2.5
import QtQuick.Controls 1.4

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("List View")

    property int number: 0

    Row {
        id: row
        height: 50
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right

        Rectangle {
            width: (parent.width / 5)
            height: 50
            Text {
                id: textIndex
                anchors.fill: parent
                text: ""
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }
        }
        Button {
            id: button1
            text: qsTr("Add button")
            width: (parent.width / 5) * 2
            height: 50

            onClicked: {
                 listModel.append({idshnik: "Button " + (++number)})
            }
        }
        Button {
            id: button2
            text: qsTr("Delete button")
            width: (parent.width / 5) * 2
            height: 50

            onClicked: {
                if(textIndex.text != ""){
                    listModel.remove(textIndex.text)
                    textIndex.text = ""
                }
            }
        }
    }

    ListView {
        id: listView1

        anchors.top: row.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right

        delegate: Item {
            id: item
            anchors.left: parent.left
            anchors.right: parent.right
            height: 40
            Button {
                anchors.fill: parent
                anchors.margins: 5

                text: idshnik

                onClicked: {
                    textIndex.text = index
                }
            }
        }

        model: ListModel {
            id: listModel
        }
    }
}
