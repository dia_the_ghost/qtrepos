#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "appcore.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QQmlApplicationEngine engine;
    appcore core;
    QQmlContext * context = engine.rootContext();
    context->setContextProperty("core", &core);

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
