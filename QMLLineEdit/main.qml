import QtQuick 2.5
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtQuick.Window 2.2

Window {
    visible: true
    width: 350;
    height: 200;
    title: qsTr("QML Text Field IP Address")

    RowLayout {
        width: 100;
        height: 100;

        anchors.fill: parent;
        anchors.margins: 10;

    Label {
        text: "Введите IP-адрес  이준기";
    }
    TextField {
        validator: RegExpValidator {
        regExp:  /^((?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\.){0,3}(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])$/
    }
    }
    }

/*
    MainForm {
        anchors.fill: parent
        mouseArea.onClicked: {
            console.log(qsTr('Clicked on background. Text: "' + textEdit.text + '"'))
        }
    }
    */
}
