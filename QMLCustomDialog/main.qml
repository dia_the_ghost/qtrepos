import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")
    color: "white"


    MainForm {
        anchors.fill: parent

        button1.style: ButtonStyle{
            background: Rectangle{
                color: control.pressed ? "#d7d7d7" : "#f7f7f7"
                border.color: "#d7d7d7"
                border.width: 2
                radius: 5
            }
            label: Text {
                text: qsTr("button 1")
                color: "black"
            }
        }

        button2.style: ButtonStyle{
            background: Rectangle{
                color: control.pressed ? "#d7d7d7" : "#f7f7f7"
                border.color: "#d7d7d7"
                border.width: 2
                radius: 5
            }
            label: Text{
                text: qsTr("button 2")
                color: "black"
            }
        }

        button1.onClicked: dialogAndroid.open();
        button2.onClicked: messageDialog.show(qsTr("Button 2 pressed"))

        // Создаём объект диалогового окна
        Dialog{
            id: dialogAndroid
            //width: 600
            //height: 500

            contentItem: Rectangle{
                width: 600
                height: 500
                color: "#f7f7f7"

                Rectangle{
                    /* Прибиваем область к левой, правой и верхней частям диалога,
                     * а также снизу к разделителю, который отделяет область от кнопок
                     */
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    color: "#f7f7f7"

                    Label{
                        id: textLabel
                        text: qsTr("Hello retard")
                        color: "#34aadc"
                        anchors.centerIn: parent
                    }

                }
                // Создаём горизонтальный разделитель с помощью Rectangle
                Rectangle{
                    id: dividerHorizontal
                    color: "#d7d7d7"
                    height: 2 // Устанавливаем ширину в два пикселя
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.bottom: row.top
                }
                /* Создаём подложку для кнопок в виде объекта Строки
                 * В данном объекте для объектов детей не работают некоторые параметры
                 * anchors, кроме параметров anchors.top и anchors.bottom
                 */

                Row{
                    id: row
                    height: 100
                    anchors.bottom: parent.bottom
                    anchors.left: parent.left
                    anchors.right: parent.right

                    Button{
                        id: dialogButtonCancel
                        // Растягиваем кнопку по высоте строки
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        // Задаём ширину кнопки на половину строки минус 1 пиксель
                        width: parent.width / 2 - 1

                        // Стилизуем кнопку
                        style: ButtonStyle{
                            background: Rectangle{
                                color: control.pressed ? "#d7d7d7" : "#f7f7f7"
                                border.width: 0
                            }
                            label: Text{
                                text: qsTr("Cancel")
                                color: "#34aadc"
                                // Устанавливаем текст в центр кнопки
                                verticalAlignment: Text.AlignVCenter
                                horizontalAlignment: Text.AlignHCenter
                            }
                        }
                        // По нажатию кнопки закрываем диалог
                        onClicked: dialogAndroid.close();
                    }
                    // Создаём разделитель между кнопками шириной в 2 пикселя
                    Rectangle{
                        id: dividerVertical
                        width: 2
                        // Растягиваем разделитель по высоте объекта строки
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        color: "#d7d7d7"
                    }

                    Button{
                        id: dialogButtonOk
                        // Растягиваем кнопку по высоте строки
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        // Задаём ширину кнопки на половину строки минус 1 пиксель
                        width: parent.width / 2 - 1

                        style: ButtonStyle{
                            background: Rectangle{
                                color: control.pressed ? "#d7d7d7" : "#f7f7f7"
                                border.width: 0
                            }

                        label: Text{
                            text: qsTr("Ok")
                            color: "#34aadc"
                            // Устанавливаем текст в центр кнопки
                            verticalAlignment: Text.AlignVCenter
                            horizontalAlignment: Text.AlignHCenter
                        }
                        }
                        onClicked: dialogAndroid.close();
                    }
                }
            }
        }
    }

    MessageDialog {
        id: messageDialog
        title: qsTr("May I have your attention, please?")

        function show(caption) {
            messageDialog.text = caption;
            messageDialog.open();
        }
    }
}
