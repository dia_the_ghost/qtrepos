import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1

ApplicationWindow {
    visible: true
    width: 480
    height: 480
    title: qsTr("Grid Layout")

    GridLayout{
        id: grid
        anchors.fill: parent

        rows: 3
        columns: 3

        Rectangle{
            color: "red"
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.columnSpan: 2
            Layout.rowSpan: 1
            Layout.row: 1
            Layout.column: 2
        }
        Rectangle{
            color: "blue"
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.columnSpan: 1
            Layout.rowSpan: 2
            Layout.row: 1
            Layout.column: 1
        }
        Rectangle{
           color: "green"
           Layout.fillHeight: true
           Layout.fillWidth: true
           Layout.columnSpan: 1
           Layout.rowSpan: 2
           Layout.row: 2
           Layout.column: 3
        }
        Rectangle{
            color: "white"
            //Layout.fillHeight: true
            //Layout.fillWidth: true
            width: 100
            height: 100
            Layout.columnSpan: 1
            Layout.rowSpan: 1
            Layout.row: 2
            Layout.column: 2
        }
        Rectangle{
            color: "yellow"
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.columnSpan: 2
            Layout.rowSpan: 1
            Layout.row: 3
            Layout.column: 1
        }
    }
}
/*
    Layout.row - указывает строку, в которой располагается объект;
    Layout.column - указывает колонку, в которой располагается объект;
    Layout.rowSpan - указывает, на сколько строк должен быть растянут объект;
    Layout.columnSpan - указывает, на сколько колонок должен быть растянут объект;
    Layout.minimumWidth - минимальная ширина объекта в слое;
    Layout.minimumHeight - минимальная высота объекта в слое;
    Layout.preferredWidth - предпочтительная ширина объекта в слое;
    Layout.preferredHeight - предпочтительная высота объекта в слое;
    Layout.maximumWidth - максимальная ширина объекта в слое;
    Layout.maximumHeight - максимальная высота объекта в слое;
    Layout.fillWidth - заполнение по ширине;
    Layout.fillHeight - заполнение по высоте;
    Layout.alignment - выравнивание в слое;

Свойства GridLayout

Также для полноты картины укажу свойства GridLayout и за что они отвечают:

    columnSpacing : real - пробелы между колонками (разрыв между объектами);
    columns : int - количество колонок;
    flow : enumeration - направление расположения объектов в GridLayout
        GridLayout.LeftToRight (default) - слева направо
        GridLayout.TopToBottom - сверху вниз
    layoutDirection : enumeration - направление перечисления объектов при заданном flow
        Qt.LeftToRight (default) - слева направо
        Qt.RightToLeft - справа налево
    rowSpacing : real - пробелы между строками (разрыв между объектами);
    rows : int - количество строк;

Если задаём:

    flow: GridLayout.TopToBottom
    layoutDirection: Qt.RightToLeft

То в результате получим, что первый элемент строки будет в самом вверху, а последний элемент строки будет в самом низу. В случае наличия двух строк, элементы первой строки будут располагаться справа.

  */
