#ifndef DATABASE_H
#define DATABASE_H

#include <QObject>
#include <QSql>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlDatabase>
#include <QFile>
#include <QDate>
#include <QDebug>

// директивы имен таблицы
#define DATABASE_HOSTNAME   "ExampleDB"
#define DATABASE_NAME       "DatabaseDB"

#define TABLE               "TableExample"
#define TABLE_DATE          "date"
#define TABLE_TIME          "time"
#define TABLE_MESSAGE       "message"
#define TABLE_RANDOM        "random"

class DataBase : public QObject
{
    Q_OBJECT
public:
    explicit DataBase(QObject *parent = 0);
    ~DataBase();

    void connectToDB();
    bool insertIntoTable(const QVariantList &data);

private:
    QSqlDatabase db;

private:
    bool openDB();
    bool restoreDB();
    void closeDB();
    bool createTable();
};

#endif // DATABASE_H
