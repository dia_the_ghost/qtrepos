#include "model.h"

Model::Model(QObject *parent):QSqlQueryModel(parent)
{
    // пуста
}

QVariant Model::data(const QModelIndex &index, int role) const
{
    // ставим номер колонки по номеру роли
    int columnId = role - Qt::UserRole - 1;
    // создаем индекс
    QModelIndex modelIndex = this->index(index.row(), columnId);

    return QSqlQueryModel::data(modelIndex, Qt::DisplayRole);
}

QHash<int, QByteArray> Model::roleNames() const
{
    // сохраняем в таблицу названия ролей по номеру
    QHash<int, QByteArray> roles;

    roles[DateRole] = "date";
    roles[TimeRole] = "time";
    roles[RandomRole] = "random";
    roles[MessageRole] = "message";

    return roles;
}
