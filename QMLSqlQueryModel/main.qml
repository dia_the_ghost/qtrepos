import QtQuick 2.5
import QtQuick.Controls 1.4

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Model")


    TableView
    {
        anchors.fill: parent

        TableViewColumn{
            role: "date"
            title: "date"
        }

        TableViewColumn{
            role: "time"
            title: "time"
        }

        TableViewColumn{
            role: "random"
            title: "random"
        }

        TableViewColumn{
            role: "message"
            title: "message"
        }

        model: myModel
    }
}
