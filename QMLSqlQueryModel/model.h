#ifndef MODEL_H
#define MODEL_H

#include <QObject>
#include <QSqlQueryModel>

class Model : public QSqlQueryModel
{
    Q_OBJECT
public:
    enum Roles {
        DateRole = Qt::UserRole + 1,
        TimeRole,
        RandomRole,
        MessageRole
    };

    explicit Model(QObject * parent = 0);
    // метод который будет возвращать данные
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole)const;

protected:
    // хэшированная таблица для ролей и колонок
    // типа он есть в QAbstractItemModel и мы кго переопределили
    QHash<int, QByteArray> roleNames() const;
};

#endif // MODEL_H
