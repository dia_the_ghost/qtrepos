import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Model View")

    RowLayout{
        id: rowLayout
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 5
        height: 30

        spacing: 5
        z: 2

        Rectangle{
            Layout.fillWidth: true
            Layout.fillHeight: true
            color: "white"

            TextInput{
                id: textInput
                anchors.fill: parent
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter

                Keys.onPressed: {
                    // 16777220 - код клавиши return
                    if(event.key === 16777220)
                    {
                        listModel.append({textList: textInput.text})
                    }
                }
            }
        }

        Button{
            id: button
            text: qsTr("добпвить")
            Layout.fillHeight: true

            onClicked: {
                listModel.append({textList: textInput.text})
            }
        }
    }

    ListView{
        id: listView

        anchors.top: rowLayout.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 5
        z: 1

        // описание внешнего вида одного элемента ListView
        delegate: Text{
            anchors.left: parent.left
            anchors.right: parent.right
            height: 50
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter

            text: textList // роль свойства text, куда будут передаваться данные
        }

        // модель
        model: ListModel{
            id: listModel
        }
    }



}
