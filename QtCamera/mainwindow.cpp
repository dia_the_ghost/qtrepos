#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    setWindowTitle("Qt Camera");
    setWindowIcon(QIcon(":/res/QtCamera/cam.png"));

    myCamera = new QCamera(this);

    myImageCapture = new QCameraImageCapture(myCamera, this);
    myRecorder = new QMediaRecorder(myCamera, this);

    vf = new QVideoWidget(this);
    i = 0;

    vf->setMinimumSize(50, 50);

    myCamera->setViewfinder(vf);
    myCamera->setCaptureMode(QCamera::CaptureViewfinder);

    this->setCentralWidget(vf);

    myCamera->start();

    myImageCapture->setCaptureDestination(QCameraImageCapture::CaptureToFile);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionTakePicture_triggered()
{
    i++;

    myCamera->setCaptureMode(QCamera::CaptureStillImage);

    myCamera->searchAndLock();
    myImageCapture->capture("C:/Users/Dia/Desktop/img_000" + QString::number(i) + ".jpg");
    myCamera->unlock();
}

void MainWindow::on_actionTakeVideo_triggered()
{
    QVideoEncoderSettings settings = myRecorder->videoSettings();
    settings.setResolution(640,480);
    settings.setQuality(QMultimedia::VeryHighQuality);
    settings.setFrameRate(30.0);

    myRecorder->setVideoSettings(settings);

    //myCamera->setCaptureMode(QCamera::CaptureVideo);
    myCamera->setCaptureMode(QCamera::CaptureViewfinder);


    QString name = "video" + QDateTime::currentDateTime().toString("dd.MM.yy-h-m-s");
    myRecorder->setOutputLocation(QUrl::fromLocalFile("C:/Users/Dia/Desktop/" + name + ".mp4"));

    myCamera->searchAndLock();
    myRecorder->record();
}


void MainWindow::on_actionStop_triggered()
{
    myRecorder->stop();
    myCamera->unlock();
}



void MainWindow::on_actionPause_triggered()
{
    myRecorder->pause();
}
