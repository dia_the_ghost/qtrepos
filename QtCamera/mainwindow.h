#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QCamera>
#include <QVideoWidget>
#include <QFile>
#include <QCameraImageCapture>
#include <QMediaRecorder>
#include <QUrl>
#include <qdebug.h>
#include <QDateTime>
#include <QFileInfo>

#include <QMediaService>
#include <QCameraViewfinder>
#include <QCameraInfo>
#include <QMediaMetaData>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionTakePicture_triggered();

    void on_actionTakeVideo_triggered();

    void on_actionStop_triggered();

    void on_actionPause_triggered();

private:
    Ui::MainWindow *ui;

    QVideoWidget *vf;

    QCamera *myCamera;
    QCameraImageCapture *myImageCapture;
    QMediaRecorder *myRecorder;

    int i;

};

#endif // MAINWINDOW_H
