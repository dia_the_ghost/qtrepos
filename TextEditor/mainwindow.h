#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QTextStream>
#include <QMessageBox>
#include <QFontDialog>

#include <QTextEdit>


#include <QFontDatabase>
#include <QTextDocumentWriter>
#include <QTextList>
#include <QMimeData>
#include <QTextCodec>
#include <QPrinter>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionSaveAs_triggered();

    void on_actionOpen_triggered();

    void on_actionSave_triggered();

    void on_actionExit_triggered();

    void on_actionNew_triggered();

    void on_actionAbout_triggered();

    void on_actionAuthors_triggered();

    void on_actionUndo_triggered();

    void on_actionRedo_triggered();


    void on_actionCopy_triggered();

    void on_actionPaste_triggered();

    void on_actionSelect_All_triggered();

    void on_actionCut_triggered();

    void on_actionItalic_triggered(bool checked);

    void on_actionUnderline_triggered(bool checked);

    void on_actionBold_triggered(bool checked);

    void on_actionQfontdialog_triggered();

    void textAlign(QAction *a);
    void currentCharFormatChanged(const QTextCharFormat &format);
    void cursorPositionChanged();
    void checkFont();
    void textChanged();

    // печать

    void printPreview(QPrinter *);
    void on_actionPrint_triggered();
    void on_actionPrintPreview_triggered();
    void on_actionExportInPdf_triggered();

    // печать

    void on_actionClose_Document_triggered();


private:
    void initAllBase();

    void setupAlignActions();

    void alignmentChanged(Qt::Alignment a);

    void iconsVerOne();
    void iconsVerTwo();


private:
    Ui::MainWindow *ui;

    QString currentFileName;
};

#endif // MAINWINDOW_H
