#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

#ifndef QT_NO_PRINTER
#include <QPrintDialog>
#include <QPrintPreviewDialog>
#endif

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    initAllBase();



    connect(ui->textEdit->document(), &QTextDocument::undoAvailable, ui->actionUndo, &QAction::setEnabled);
    connect(ui->textEdit->document(), &QTextDocument::redoAvailable, ui->actionRedo, &QAction::setEnabled);
    connect(ui->textEdit, &QTextEdit::textChanged, this, &MainWindow::textChanged);

    connect(ui->textEdit, &QTextEdit::currentCharFormatChanged, this, &MainWindow::currentCharFormatChanged);
    connect(ui->textEdit, &QTextEdit::cursorPositionChanged, this, &MainWindow::cursorPositionChanged);

    setupAlignActions();
}


MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initAllBase()
{
    setWindowTitle(tr("Text Editor =)"));
    ui->actionUndo->setEnabled(false);
    ui->actionRedo->setEnabled(false);
    ui->actionSave->setEnabled(false);


    //QPixmap pixmap(":/res/thin-round-icons.gif");

    //Вырезаем QPixmap по рамке x, y, ширина, высота (в пикселях)

    //QPixmap openPixmap = pixmap.copy(48, 16, 70, 70);
    //ui->actionOpen->setIcon(QIcon(openPixmap));

    //ui->actionSave->setIcon(QIcon(pixmap.copy(136, 91, 70, 70)));

    iconsVerOne();


    ui->actionClose_Document->setIcon(this->style()->standardIcon((QStyle::SP_DialogCloseButton)));
}

void MainWindow::iconsVerOne()
{
    ui->actionUndo->setIcon(QIcon(":/res/ver1/Undo.ico"));
    ui->actionRedo->setIcon(QIcon(":/res/ver1/Redo.ico"));

    ui->actionOpen->setIcon(QIcon(":/res/ver1/open.ico"));
    ui->actionSave->setIcon(QIcon(":/res/ver1/save.ico"));

    ui->actionAlignCenter->setIcon(QIcon(":/res/ver1/AlignCenter.ico"));
    ui->actionAlignJustify->setIcon(QIcon(":/res/ver1/AlignJustify.ico"));
    ui->actionAlignLeft->setIcon(QIcon(":/res/ver1/AlignLeft.ico"));
    ui->actionAlignRight->setIcon(QIcon(":/res/ver1/AlignRight.ico"));

    ui->actionBold->setIcon(QIcon(":/res/ver1/Bold.ico"));
    ui->actionItalic->setIcon(QIcon(":/res/ver1/Italic.ico"));
    ui->actionUnderline->setIcon(QIcon(":/res/ver1/Underline.ico"));
}

void MainWindow::iconsVerTwo()
{
    ui->actionUndo->setIcon(QIcon(":/res/ver2/Undo2.ico"));
    ui->actionRedo->setIcon(QIcon(":/res/ver2/Redo2.ico"));

    ui->actionOpen->setIcon(QIcon(":/res/ver2/Open2.ico"));
    ui->actionSave->setIcon(QIcon(":/res/ver2/Save2.ico"));

    ui->actionAlignCenter->setIcon(QIcon(":/res/ver2/AlignCenter2.ico"));
    ui->actionAlignJustify->setIcon(QIcon(":/res/ver2/AlignJustify2.ico"));
    ui->actionAlignLeft->setIcon(QIcon(":/res/ver2/AlignLeft2.ico"));
    ui->actionAlignRight->setIcon(QIcon(":/res/ver2/AlignRight2.ico"));

    ui->actionBold->setIcon(QIcon(":/res/ver2/Bold2.ico"));
    ui->actionItalic->setIcon(QIcon(":/res/ver2/Italic2.ico"));
    ui->actionUnderline->setIcon(QIcon(":/res/ver2/Underline2.ico"));
}


void MainWindow::setupAlignActions()
{
    QActionGroup *alignGroup = new QActionGroup(this);
    connect(alignGroup, &QActionGroup::triggered, this, &MainWindow::textAlign);

    if (QApplication::isLeftToRight())
    {
        alignGroup->addAction(ui->actionAlignLeft);
        alignGroup->addAction(ui->actionAlignCenter);
        alignGroup->addAction(ui->actionAlignRight);
    }
    else
    {
        alignGroup->addAction(ui->actionAlignRight);
        alignGroup->addAction(ui->actionAlignCenter);
        alignGroup->addAction(ui->actionAlignRight);
    }

    alignGroup->addAction(ui->actionAlignJustify);
}


void MainWindow::alignmentChanged(Qt::Alignment a)
{
    if(a & Qt::AlignLeft)
        ui->actionAlignLeft->setChecked(true);

    else if(a & Qt::AlignCenter)
        ui->actionAlignCenter->setChecked(true);

    else if(a & Qt::AlignRight)
        ui->actionAlignRight->setChecked(true);

    else if(a & Qt::AlignJustify)
        ui->actionAlignJustify->setChecked(true);
}

// РАБОТАЕТ ПРАВИЛЬНО

void MainWindow::on_actionSave_triggered()
{
    if (currentFileName == "")
    {
        QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"), "",
        tr("Text Files (*.txt);;C++ Files (*.cpp *.h)"));

        currentFileName = fileName;
    }


    QTextDocumentWriter writer(currentFileName);
    writer.setFormat("plaintext");
    bool success = writer.write(ui->textEdit->document());
    if (success)
    {
        ui->textEdit->document()->setModified(false);
        ui->actionSave->setEnabled(false);
    }

}

void MainWindow::on_actionSaveAs_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"), "",
    tr("Text Files (*.txt);;C++ Files (*.cpp *.h)"));

    currentFileName = fileName;

    //вызываем save();
    this->on_actionSave_triggered();

}



void MainWindow::on_actionOpen_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,tr("OpenFile"),"",tr("Text Files (*.txt);;C++ Files (*.cpp *.h);;ODT (*.odt)"));
    if (fileName != "")
    {
        currentFileName = fileName;
        qDebug() << currentFileName;

        QFile file(fileName);
        if (!file.open(QIODevice::ReadOnly))
        {
            QMessageBox::critical(this,tr("Error"),tr("Could not open file"));
            return;
        }

        ///////////////////////////////////////////////////////////////////////////////


        QByteArray data = file.readAll();

// native
        QTextCodec * codec = QTextCodec::codecForUtfText(data);
        QString str = codec->fromUnicode(data);
        str = QString::fromUtf8(data);


//common
         //QTextCodec * codec = QTextCodec::codecForHtml(data);
         //QString str = codec->toUnicode(data);
         //str = QString::fromLocal8Bit(data);


         ui->textEdit->setPlainText(str);

         file.close();

//////////////////////////////////////////////////////////////////////////////////////

//        QTextStream in(&file);
//        ui->textEdit->setText(in.readAll());
//        file.close();
    }
}



void MainWindow::on_actionExit_triggered()
{
    //задать вопрос что делать с файлом

    on_actionSave_triggered();
    close();
}

void MainWindow::on_actionNew_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"), "",
                                                    tr("Text Files (*.txt);;C++ Files (*.cpp *.h)"));

    if (fileName != "")
    {
        currentFileName = fileName;

        QFile file(fileName);
        if (!file.open(QIODevice::ReadWrite))
        {
            QMessageBox::critical(this,tr("Error"),tr("Could not open file"));
            return;
        }
        QTextStream in(&file);
        ui->textEdit->setText(in.readAll());
        file.close();
    }
}

void MainWindow::on_actionAbout_triggered()
{
    QMessageBox::aboutQt(this, tr("About Qt"));
}

void MainWindow::on_actionAuthors_triggered()
{
    QMessageBox::about(this, tr("Authors"), tr("Автор: Диана Волкова. <br> Дизайнер кривых иконок: C-C-COMBO"));
}





// UNDO REDO


void MainWindow::on_actionUndo_triggered()
{
    ui->textEdit->undo();
}

void MainWindow::on_actionRedo_triggered()
{
    ui->textEdit->redo();
}


// COPY PASTE
void MainWindow::on_actionCopy_triggered()
{
    ui->textEdit->copy();
}

void MainWindow::on_actionPaste_triggered()
{
    ui->textEdit->paste();
}

// SELECT ALL CUT

void MainWindow::on_actionSelect_All_triggered()
{
    ui->textEdit->selectAll();
}

void MainWindow::on_actionCut_triggered()
{
    ui->textEdit->cut();
}
// font

void MainWindow::on_actionItalic_triggered(bool checked)
{
    ui->textEdit->setFontItalic(checked);
}

void MainWindow::on_actionUnderline_triggered(bool checked)
{
    ui->textEdit->setFontUnderline(checked);
}

void MainWindow::on_actionBold_triggered(bool checked)
{
    if (checked)    ui->textEdit->setFontWeight(QFont::DemiBold);
    else            ui->textEdit->setFontWeight(QFont::Normal);
}

//вызываем окошко в котором ставим размер и шрифт

void MainWindow::on_actionQfontdialog_triggered()
{
    ui->textEdit->setFont(QFontDialog::getFont(0, ui->textEdit->font()));
}

// ALIGNMENT

void MainWindow::textAlign(QAction *a)
{
    if(a == ui->actionAlignLeft)
        ui->textEdit->setAlignment(Qt::AlignLeft | Qt::AlignAbsolute);

    else if(a == ui->actionAlignCenter)
        ui->textEdit->setAlignment(Qt::AlignCenter);

    else if(a == ui->actionAlignRight)
        ui->textEdit->setAlignment(Qt::AlignRight | Qt::AlignAbsolute);

    else if(a == ui->actionAlignJustify)
        ui->textEdit->setAlignment(Qt::AlignJustify);
}

void MainWindow::currentCharFormatChanged(const QTextCharFormat &format)
{
    //
    Q_UNUSED(format)

}

void MainWindow::cursorPositionChanged()
{
    alignmentChanged(ui->textEdit->alignment());

    checkFont();
}

void MainWindow::checkFont()
{
    ui->textEdit->fontItalic() ? ui->actionItalic->setChecked(true) : ui->actionItalic->setChecked(false);
    ui->textEdit->fontUnderline() ? ui->actionUnderline->setChecked(true) : ui->actionUnderline->setChecked(false);

    if(ui->textEdit->fontWeight() == QFont::DemiBold)
        ui->actionBold->setChecked(true);

    else if(ui->textEdit->fontWeight() == QFont::Normal)
        ui->actionBold->setChecked(false);
}

void MainWindow::textChanged()
{
    ui->actionSave->setEnabled(true);
}

//-------------------------------------ПЕЧАТЬ----------------------------------------------
void MainWindow::printPreview(QPrinter * printer)
{
#ifdef QT_NO_PRINTER
    Q_UNUSED(printer);
#else
    ui->textEdit->print(printer);
#endif
}


void MainWindow::on_actionPrintPreview_triggered()
{
#if !defined(QT_NO_PRINTER) && !defined(QT_NO_PRINTDIALOG)
    QPrinter printer(QPrinter::HighResolution);
    QPrintPreviewDialog preview(&printer, this);

    connect(&preview, &QPrintPreviewDialog::paintRequested, this, &MainWindow::printPreview);
    preview.exec();
#endif
}


void MainWindow::on_actionPrint_triggered()
{
#if !defined(QT_NO_PRINTER) && !defined(QT_NO_PRINTDIALOG)
    QPrinter printer(QPrinter::HighResolution);
    QPrintDialog * dlg = new QPrintDialog(&printer, this);

    if(ui->textEdit->textCursor().hasSelection())
        dlg->addEnabledOption(QAbstractPrintDialog::PrintSelection);
    dlg->setWindowTitle(tr("Print document"));

    if(dlg->exec() == QDialog::Accepted)
        ui->textEdit->print(&printer);

    delete dlg;

#endif
}

//-------------------------------------ПЕЧАТЬ----------------------------------------------


void MainWindow::on_actionClose_Document_triggered()
{
    // сохраняем перед выходом
    on_actionSave_triggered();

    // очищаем currentFileName
    currentFileName = "";

    // очищаем textEdit
    ui->textEdit->clear();
}

void MainWindow::on_actionExportInPdf_triggered()
{
    QFileDialog fileDialog(this, tr("Export PDF"));
    fileDialog.setAcceptMode(QFileDialog::AcceptSave);
    fileDialog.setMimeTypeFilters(QStringList("application/pdf"));
    fileDialog.setDefaultSuffix("pdf");

    if (fileDialog.exec() != QDialog::Accepted) return;

    QString fileName = fileDialog.selectedFiles().first();
    QPrinter printer(QPrinter::HighResolution);
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setOutputFileName(fileName);
    ui->textEdit->document()->print(&printer);

    QMessageBox::information(this,tr("Done!"),tr("Export is finished"));
}
