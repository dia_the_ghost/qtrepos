#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    camera = new QCamera(this);
    viewFinder = new QCameraViewfinder(this);
    camera->setViewfinder(viewFinder);
    recorder = new QMediaRecorder(camera,this);

    vf = new QVideoWidget(this);
    vf->setMinimumSize(50, 50);
    this->setCentralWidget(vf);
    camera->setViewfinder(vf);

    //camera->setCaptureMode(QCamera::CaptureVideo);
    camera->setCaptureMode(QCamera::CaptureViewfinder);
    camera->start();

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionRecord_triggered()
{
    //QString outputpath = "C:/Users/Dia/Desktop/";
    QString name = "filename" + QDateTime::currentDateTime().toString("dd.MM.yy-h-m-s");
    recorder->setOutputLocation(QUrl::fromLocalFile(name + ".mp4"));
    recorder->record();
}
