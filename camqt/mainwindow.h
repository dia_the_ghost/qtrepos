#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QCamera>
#include <QVideoWidget>
#include <QFile>
#include <QMediaRecorder>
#include <QUrl>
#include <QDateTime>
#include <QFileInfo>

#include <QMediaService>
#include <QCameraViewfinder>
#include <QCameraInfo>
#include <QMediaMetaData>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionRecord_triggered();

private:
    Ui::MainWindow *ui;

    QVideoWidget *vf;

    QCamera *camera;
    QCameraViewfinder *viewFinder;
    QMediaRecorder *recorder;

};

#endif // MAINWINDOW_H
