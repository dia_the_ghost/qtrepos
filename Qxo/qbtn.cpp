#include "qbtn.h"

QBtn::QBtn(int i, int j, QWidget *parent) : QPushButton(parent)
  , _i(i), _j(j)
{
    setMinimumSize(40, 40);
    setSizePolicy(QSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding));
}

int QBtn::i()
{
    return _i;
}

int QBtn::j()
{
    return _j;
}

void QBtn::setO()
{
    if(this->isEmpty())
    {
        this->setText("O");
    }
    else
    {
        QMessageBox::information(this, "message", "выберите другую кнопку");
    }
}

void QBtn::setX()
{
    if(this->isEmpty())
    {
        this->setText("X");
    }
    else
    {
        QMessageBox::information(this, "message", "выберите другую кнопку");
    }
}

bool QBtn::isEmpty()
{
    if(this->text() == "")
        return true;
    else
        return false;
}
