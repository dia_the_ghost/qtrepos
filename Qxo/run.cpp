#include "run.h"
#include <QDebug>

Run::Run(QWidget *parent) : QWidget(parent)
{
    // инициализируем сетку
    grid = new QGridLayout(this);
    grid->setHorizontalSpacing(1);
    grid->setVerticalSpacing(1);

    step = true;
    n = 0;

    // заполняем кнопками
    for(int i(0); i < 3; ++i)
    {
        for(int j(0); j < 3; ++j)
        {
            QBtn * btn = new QBtn(i, j, this);
            connect(btn, SIGNAL(clicked()), this, SLOT(setXO()));
            buttons << btn;
            grid->addWidget(btn, i, j);
        }
    }

    setLayout(grid);

    //resize(sizeHint());
}

void Run::setXO()
{
    // адрес кнопки по которой кликнули передем в новую кнопку
    QBtn* clickedBtn = static_cast<QBtn*>(sender());

    //  и ставим Х или О    
    if(step)
       {
        // если кликнутая кнопка уже занята, выходим

        if(!clickedBtn->isEmpty()) return;

        clickedBtn->setX();
        step = false;
       }

    if(!step)
       {
           AI();
           step =  true;
       }

    checkFinished();
}

void Run::AI()
{

    // от 0 до 8
    int da[24] = {0, 0, 1,  3, 3, 5,  6, 6, 8,  6, 0, 6,  1, 1, 4,  2, 2, 8,  2, 2, 6,  0, 0, 8};
    int db[24] = {1, 2, 2,  4, 5, 4,  7, 8, 7,  3, 3, 0,  4, 7, 7,  5, 8, 5,  4, 6, 4,  4, 8, 4};
    int dc[24] = {2, 1, 0,  5, 4, 3,  8, 7, 6,  0, 6, 3,  7, 4, 1,  8, 5, 2,  6, 4, 2,  8, 4, 0};


    for(int index(0); index < 24; ++index)
    {
        if(!buttons.at(da[index])->isEmpty() && !buttons.at(db[index])->isEmpty())
        {
            if(buttons.at(da[index])->text() == buttons.at(db[index])->text() && (buttons.at(dc[index])->isEmpty()))
            {
                buttons.at(dc[index])->setO();
                return;
            }
        }
    }

    // если еще есть пустые ячейки (компу есть куда ходить)

    if (countFreeCelles() >= 1)
    {
        int d;
        qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));

        do d = qrand() % 8;
        while(!buttons.at(d)->isEmpty());

        buttons.at(d)->setO();
    }

}

//int Run::minimaxAI(bool step, int n)
//{
//    int res;
//    int i;
//    int max =- INFINITY;
//    int min = INFINITY;

//    int chk = checkFinished();  // 0 1 -1
//    if (chk == 1){ return 1;}
//    else if (chk == -1){ return -1;}
//    else if (chk == 0) { return 0; }

//    for(i = 1; i <= 9; ++i)
//    {
//        if(buttons.at(i)->isEmpty())
//        {
//            // ходит ребекка
//            if (step == false)
//            {
//                // ставим O
//                buttons.at(i)->setO();
//                res = minimaxAI(true, n + 1);
//                buttons.at(i)->setText("");
//                if(res < min)
//                {
//                    min = res;
//                    if(n == 0) posmin = i;
//                }
//            }

//            // ходит человек
//            else if(step == true)
//            {
//                // ставим X
//                setXO();
//                res = minimaxAI(false, n + 1);
//                buttons.at(i)->setText("");
//                if(res > max)
//                {
//                    max = res;
//                    if(n == 0) posmax = i;
//                }
//            }
//        }
//    }



//    if(step) return max;
//    else return min;

//}

int Run::checkFinished()
{
    /// Win если на кнопках стоит X или O
    // 00 01 02             0 1 2
    // 10 11 12             3 4 5
    // 20 21 22             6 7 8

    if((buttons.at(0)->text() == "X") && (buttons.at(1)->text() == "X") && (buttons.at(2)->text() == "X"))
    {
        QMessageBox::information(this, "message", "Победил X");
        blockButtons();
        return 1;
    }

    else if((buttons.at(3)->text() == "X") && (buttons.at(4)->text() == "X") && (buttons.at(5)->text() == "X"))
    {
        QMessageBox::information(this, "message", "Победил X");
        blockButtons();
        return 1;
    }

    else if((buttons.at(6)->text() == "X") && (buttons.at(7)->text() == "X") && (buttons.at(8)->text() == "X"))
    {
        QMessageBox::information(this, "message", "Победил X");
        blockButtons();
        return 1;
    }

    // 00 10 20             0 3 6
    // 01 11 21             1 4 7
    // 02 12 22             2 5 8

    else if((buttons.at(0)->text() == "X") && (buttons.at(3)->text() == "X") && (buttons.at(6)->text() == "X"))
    {
        QMessageBox::information(this, "message", "Победил X");
        blockButtons();
        return 1;
    }

    else if((buttons.at(1)->text() == "X") && (buttons.at(4)->text() == "X") && (buttons.at(7)->text() == "X"))
    {
        QMessageBox::information(this, "message", "Победил X");
        blockButtons();
        return 1;
    }

    else if((buttons.at(2)->text() == "X") && (buttons.at(5)->text() == "X") && (buttons.at(8)->text() == "X"))
    {
        QMessageBox::information(this, "message", "Победил X");
        blockButtons();
        return 1;
    }

    // 00 11 22             0 4 8
    // 02 11 20             2 4 6

    else if((buttons.at(0)->text() == "X") && (buttons.at(4)->text() == "X") && (buttons.at(8)->text() == "X"))
    {
        QMessageBox::information(this, "message", "Победил X");
        blockButtons();
        return 1;
    }

    else if((buttons.at(2)->text() == "X") && (buttons.at(4)->text() == "X") && (buttons.at(6)->text() == "X"))
    {
        QMessageBox::information(this, "message", "Победил X");
        blockButtons();
        return 1;
    }


    //// та эе хуета для плеера 2
    /// Win если на кнопках стоит X или O
    // 00 01 02             0 1 2
    // 10 11 12             3 4 5
    // 20 21 22             6 7 8

    if((buttons.at(0)->text() == "O") && (buttons.at(1)->text() == "O") && (buttons.at(2)->text() == "O"))
    {
        QMessageBox::information(this, "message", "Победил O");
        blockButtons();
        return -1;
    }

    else if((buttons.at(3)->text() == "O") && (buttons.at(4)->text() == "O") && (buttons.at(5)->text() == "O"))
    {
        QMessageBox::information(this, "message", "Победил O");
        blockButtons();
        return -1;
    }

    else if((buttons.at(6)->text() == "O") && (buttons.at(7)->text() == "O") && (buttons.at(8)->text() == "O"))
    {
        QMessageBox::information(this, "message", "Победил O");
        blockButtons();
        return -1;
    }

    // 00 10 20             0 3 6
    // 01 11 21             1 4 7
    // 02 12 22             2 5 8

    else if((buttons.at(0)->text() == "O") && (buttons.at(3)->text() == "O") && (buttons.at(6)->text() == "O"))
    {
        QMessageBox::information(this, "message", "Победил O");
        blockButtons();
        return -1;
    }

    else if((buttons.at(1)->text() == "O") && (buttons.at(4)->text() == "O") && (buttons.at(7)->text() == "O"))
    {
        QMessageBox::information(this, "message", "Победил O");
        blockButtons();
        return -1;
    }

    else if((buttons.at(2)->text() == "O") && (buttons.at(5)->text() == "O") && (buttons.at(8)->text() == "O"))
    {
        QMessageBox::information(this, "message", "Победил O");
        blockButtons();
        return -1;
    }

    // 00 11 22             0 4 8
    // 02 11 20             2 4 6

    else if((buttons.at(0)->text() == "O") && (buttons.at(4)->text() == "O") && (buttons.at(8)->text() == "O"))
    {
        QMessageBox::information(this, "message", "Победил O");
        blockButtons();
        return -1;
    }

    else if((buttons.at(2)->text() == "O") && (buttons.at(4)->text() == "O") && (buttons.at(6)->text() == "O"))
    {
        QMessageBox::information(this, "message", "Победил O");
        blockButtons();
        return -1;
    }

    /// НИЧЬЯ
    // проходимся фором и проверяем что все кнопки не пусты
    else
    {
        int o = 0;

        for(QList<QBtn*>::const_iterator it = buttons.begin(); it != buttons.end(); ++it)
        {
            if(!(*it)->isEmpty())
            {
                o++;
            }
        }
        if(o == buttons.count())
        {
            QMessageBox::information(this, "message", "Ничья");
            blockButtons();
            return 0;
        }
    }
    return 0;

}

void Run::blockButtons()
{
    for(QList<QBtn*>::const_iterator it = buttons.begin(); it != buttons.end(); ++it)
        (*it)->setDisabled(true);
}

int Run::countFreeCelles()
{
    int celles = 0;

    for(int i(0); i < 9 ;++i)
    {
        if(buttons.at(i)->isEmpty()) celles = i;
    }
    return celles;
}
