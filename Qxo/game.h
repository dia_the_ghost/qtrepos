#ifndef GAME_H
#define GAME_H

#include <QWidget>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLabel>
#include "run.h"

class Game : public QWidget
{
    Q_OBJECT

public:
    Game(QWidget *parent = 0);
    ~Game();

private:
    QVBoxLayout *layout;
    Run * run;

private slots:
    void startNewGame();
};

#endif // GAME_H
