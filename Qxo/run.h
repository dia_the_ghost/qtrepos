#ifndef RUN_H
#define RUN_H

#include <QWidget>
#include <QGridLayout>
#include <QTime>
#include "qbtn.h"

class Run : public QWidget
{
    Q_OBJECT
public:
    explicit Run(QWidget *parent = 0);

private:
    QString player1;
    QString player2;

    bool step;
    QList <QBtn * > buttons;            // ячейки
    QGridLayout* grid;                  // сетка, куда мы будем пихать ячейки

    int n;
    int posmin;
    int posmax;


private slots:
    void setXO();
    void AI();
    //int minimaxAI(bool step, int n);
    int checkFinished();
    void blockButtons();

    int countFreeCelles();

};

#endif // RUN_H
