#-------------------------------------------------
#
# Project created by QtCreator 2017-03-04T13:50:43
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Qxo
TEMPLATE = app


SOURCES += main.cpp\
        game.cpp \
    run.cpp \
    qbtn.cpp

HEADERS  += game.h \
    run.h \
    qbtn.h

CONFIG += mobility
MOBILITY = 

