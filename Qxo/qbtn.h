#ifndef QBTN_H
#define QBTN_H

#include <QPushButton>
#include <QMessageBox>


class QBtn : public QPushButton
{
    Q_OBJECT
public:
    QBtn(int i, int j, QWidget *parent = 0);

    int i();                  //!< возвращает индекс строки кнопки
    int j();                  //!< возвращает индекс столбца кнопки

    bool isEmpty();

    bool player;

public slots:
    void setX();             //!< пишет в кнопку х или о
    void setO();

private:
    int _i;
    int _j;

};

#endif // QBTN_H
