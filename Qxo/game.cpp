#include "game.h"

Game::Game(QWidget *parent)
    : QWidget(parent)
{
    layout = new QVBoxLayout(this);
    this->setLayout(layout);
    this->setMinimumSize(400, 400);

    run = new Run(0);


    // кнопка рестарта игры
    QPushButton * restart = new QPushButton(this);
    restart->setText("Retsart");
    restart->setMinimumHeight(30);
    connect(restart, SIGNAL(clicked()), this, SLOT(startNewGame()));
    layout->addWidget(restart);

    // запускаем игру из конструктора
    startNewGame();
}

Game::~Game()
{
    delete run;
}

void Game::startNewGame()
{
    if(run)
    {
        layout->removeWidget(run);
        delete run;
    }

    run = new Run (this);
    layout->addWidget(run);
    layout->update();

    //resize(sizeHint());
}
