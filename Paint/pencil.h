﻿#ifndef PENCIL_H
#define PENCIL_H

#include <QObject>
#include <QGraphicsItem>
#include <QPainter>
#include "figure.h"


class Pencil : public Figure
{
    Q_OBJECT
public:
    explicit Pencil(QPointF point, QPen pen, QObject * parent = 0);
    ~Pencil();

private:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
};

#endif // PENCIL_H
