#ifndef CIRCLE_H
#define CIRCLE_H

#include <QObject>
#include <QGraphicsItem>
#include <QPainter>
#include "figure.h"


class Circle : public Figure
{
    Q_OBJECT
public:
    explicit Circle(QPointF point, QPen pen, QObject * parent = 0);
    ~Circle();
private:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
};

#endif // CIRCLE_H
