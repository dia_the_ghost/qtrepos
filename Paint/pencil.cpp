#include "pencil.h"

Pencil::Pencil(QPointF point, QPen pen, QObject *parent) : Figure(point, pen, parent)
{
    Q_UNUSED(point)
    Q_UNUSED(pen)
}

Pencil::~Pencil()
{

}

void Pencil::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setPen(this->pen());

    //painter->drawPoint(startPoint().x(), startPoint().y());
    //addEllipse(event->scenePos().x() - 5, event->scenePos().y() - 5, 0, 0, QPen(pen), QBrush(Qt::white));
    painter->drawEllipse(startPoint().x() - 5, startPoint().y() - 5, 0, 0);

    Q_UNUSED(widget)
    Q_UNUSED(option)
}
