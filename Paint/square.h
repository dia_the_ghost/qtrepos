#ifndef SQUARE_H
#define SQUARE_H

#include <QObject>
#include <QGraphicsItem>
#include <QPainter>
#include "figure.h"


class Square : public Figure
{
    Q_OBJECT
public:
    explicit Square(QPointF point, QPen pen, QObject * parent = 0);
    ~Square();
private:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
};

#endif // SQUARE_H
