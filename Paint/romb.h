#ifndef ROMB_H
#define ROMB_H

#include <QObject>
#include <QGraphicsItem>
#include <QPainter>
#include "figure.h"


class Romb : public Figure
{
    Q_OBJECT
public:
    explicit Romb(QPointF point, QPen pen, QObject * parent = 0);
    ~Romb();
private:
    // для ромба реализуем только саму отрисовку
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
};

#endif // ROMB_H
