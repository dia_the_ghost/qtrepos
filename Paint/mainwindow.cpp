#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->setWindowTitle("QPaint");

    scene = new PaintScene();
    num = 5;

    ui->graphicsView->setScene(scene);
    ui->graphicsView->setRenderHint(QPainter::Antialiasing);
    ui->graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    ui->graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);

    timer = new QTimer();       // Инициализируем таймер
    connect(timer, &QTimer::timeout, this, &MainWindow::slotTimer);
    timer->start(100);          // Запускаем таймер

    connect(ui->spinBox, SIGNAL(valueChanged(int)), ui->spinBox, SLOT(setValue(int)));
    //connect(ui->spinBox, SIGNAL(valueChanged(int)), ui->horizontalSlider, SLOT(setValue(int)));
    //connect(ui->horizontalSlider, SIGNAL(valueChanged(int)), ui->spinBox, SLOT(setValue(int)));
    //valueChanged(), setColor(previousColor)
    connect(ui->spinBox, SIGNAL(valueChanged(int)), this, SLOT(slotResetPen()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

int MainWindow::getNum() const
{
    return num;
}

void MainWindow::setNum(int value)
{
    num = value;
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
    timer->start();
    QMainWindow::resizeEvent(event);
}

void MainWindow::slotResetPen()
{
    scene->setPen(QPen(scene->getPen().color(), getNum(), Qt::SolidLine, scene->getPen().capStyle()));
}

void MainWindow::slotTimer()
{
    timer->stop();
    scene->setSceneRect(0, 0, ui->graphicsView->width() - 5, ui->graphicsView->height() - 5);
}

void MainWindow::on_buttonPolygon_clicked()
{
    scene->setTypeFigure(PaintScene::RombType);
}

void MainWindow::on_buttonTriangle_clicked()
{
    scene->setTypeFigure(PaintScene::TriangleType);
}

void MainWindow::on_buttonSquare_clicked()
{
    scene->setTypeFigure(PaintScene::SquareType);
}

void MainWindow::on_buttoLine_clicked()
{
    scene->setTypeFigure(PaintScene::LineType);
}
void MainWindow::on_buttonCircle_clicked()
{
    scene->setTypeFigure(PaintScene::CircleType);
}


void MainWindow::on_buttonClear_clicked()
{
    scene->clear();
    scene->update(QRectF(0, 0, this->width(), this->height()));
}

void MainWindow::on_buttonPencil_clicked()
{
    scene->setTypeFigure(PaintScene::PencilType);
}

void MainWindow::on_buttonRed_clicked()
{
    scene->setPen(QPen(Qt::red, getNum(), Qt::SolidLine, Qt::RoundCap));
}

void MainWindow::on_buttonGreen_clicked()
{
    scene->setPen(QPen(Qt::green, getNum(), Qt::SolidLine, Qt::RoundCap));
}

void MainWindow::on_buttonBlue_clicked()
{
    scene->setPen(QPen(Qt::blue, getNum(), Qt::SolidLine, Qt::RoundCap));
}

void MainWindow::on_buttonYellow_clicked()
{
    scene->setPen(QPen(Qt::yellow, getNum(), Qt::SolidLine, Qt::RoundCap));
}

void MainWindow::on_buttonOrange_clicked()
{
    scene->setPen(QPen(QColor(255, 85, 0), getNum(), Qt::SolidLine, Qt::RoundCap));
}

void MainWindow::on_buttonLightblue_clicked()
{
    scene->setPen(QPen(QColor(85, 170, 255), getNum(), Qt::SolidLine, Qt::RoundCap));
}

void MainWindow::on_buttonViolet_clicked()
{
    scene->setPen(QPen(QColor(170, 85, 255), getNum(), Qt::SolidLine, Qt::RoundCap));
}

void MainWindow::on_buttonBlack_clicked()
{
    scene->setPen(QPen(Qt::black, getNum(), Qt::SolidLine, Qt::RoundCap));
}

void MainWindow::on_buttonTransparent_clicked()
{
    scene->setPen(QPen(Qt::white, getNum(), Qt::SolidLine, Qt::RoundCap));
}

void MainWindow::on_spinBox_valueChanged(int arg1)
{
    this->setNum(arg1);

    //currentCheckboxReactivate();
    //on_buttonBlack_clicked();
}

void MainWindow::on_buttonSave_clicked()
{
    QString format = "png";
    QString initialPath = QDir::currentPath() + tr("/untitled.") + format;

    QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"),
                                                    initialPath,
                                                    tr("%1 Files (*.%2);;All Files (*)")
                                                    .arg(format.toUpper())
                                                    .arg(format));

    // без фона

    //QImage image (ui->graphicsView->width(), ui->graphicsView->height(), QImage::Format_ARGB32_Premultiplied);
    QImage image (scene->width(), scene->height(), QImage::Format_ARGB32_Premultiplied);
    QPainter painter(&image);
    scene->render(&painter);
    image.save(fileName, "PNG");
}

void MainWindow::on_buttonmoreColors_clicked()
{
    QColorDialog * dlg = new QColorDialog(this);
    dlg->setOption(QColorDialog::ShowAlphaChannel, true);
    dlg->setOption(QColorDialog::DontUseNativeDialog, true);
    dlg->exec();

    scene->setPen(QPen(dlg->currentColor(), getNum(), Qt::SolidLine, Qt::RoundCap));
}

void MainWindow::on_buttonOpen_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "/", tr("Images (*.png *.xpm *.jpg)"));

    QPixmap image(fileName);
    scene->clear();
    scene->addPixmap(image);
    ui->graphicsView->setSceneRect(0, 0, image.width(), image.height());
    //scene->update(QRectF(0, 0, this->widthMM(), this->heightMM()));
}

void MainWindow::on_actionOpen_triggered()
{
    this->on_buttonOpen_clicked();
}

void MainWindow::on_actionSave_triggered()
{
    this->on_buttonSave_clicked();
}

void MainWindow::on_actionAbout_triggered()
{
}

void MainWindow::on_actionAuthors_triggered()
{
    QMessageBox::about(this, tr("Authors"), tr("Автор: Диана Волкова. <br> dia.the.ghost@gmail.com"));
}
