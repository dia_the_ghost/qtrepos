#-------------------------------------------------
#
# Project created by QtCreator 2017-01-07T08:55:52
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Paint
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    paintscene.cpp \
    figure.cpp \
    romb.cpp \
    triangle.cpp \
    square.cpp \
    line.cpp \
    pencil.cpp \
    circle.cpp

HEADERS  += mainwindow.h \
    paintscene.h \
    figure.h \
    romb.h \
    triangle.h \
    square.h \
    line.h \
    pencil.h \
    circle.h

FORMS    += mainwindow.ui

CONFIG += mobility
MOBILITY = 

