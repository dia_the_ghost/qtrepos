#include "triangle.h"

Triangle::Triangle(QPointF point, QPen pen, QObject *parent) : Figure(point, pen, parent)
{
    Q_UNUSED(point)
    Q_UNUSED(pen)
}

Triangle::~Triangle()
{

}

void Triangle::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    // ЗАМЕНИТЬ
    painter->setPen(this->pen());

    QPolygonF polygon;

    polygon << QPointF(startPoint().x() + (endPoint().x() > startPoint().x() ? + 1 : - 1)*
                       abs((endPoint().x() - startPoint().x())/2), startPoint().y())
            << QPointF((endPoint().x() > startPoint().x()) ? endPoint().x() : startPoint().x(),
                       endPoint().y())
            << QPointF((endPoint().x() > startPoint().x()) ? startPoint().x() : endPoint().x(),
                       endPoint().y());

    painter->drawPolygon(polygon);

    Q_UNUSED(widget)
    Q_UNUSED(option)

}
