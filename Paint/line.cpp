#include "line.h"

Line::Line(QPointF point, QPen pen, QObject *parent) : Figure(point, pen, parent)
{
    Q_UNUSED(point)
    Q_UNUSED(pen)
}

Line::~Line()
{

}

void Line::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setPen(this->pen());

    QLine line(startPoint().x(), startPoint().y(), endPoint().x(), endPoint().y());

    painter->drawLine(line);

    Q_UNUSED(widget)
    Q_UNUSED(option)
}
