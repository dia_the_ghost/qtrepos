#include "paintscene.h"
#include "romb.h"
#include "square.h"
#include "triangle.h"
#include "line.h"
#include "pencil.h"
#include "circle.h"

PaintScene::PaintScene(QObject *parent) : QGraphicsScene(parent)
{
    setPen(QPen(Qt::black, 5, Qt::SolidLine, Qt::RoundCap));
    this->setTypeFigure(PencilType);
}

PaintScene::~PaintScene()
{

}

int PaintScene::typeFigure() const
{
    return m_typeFigure;
}

void PaintScene::setTypeFigure(const int type)
{
    m_typeFigure = type;
}

QPen PaintScene::getPen() const
{
    return pen;
}

void PaintScene::setPen(const QPen &value)
{
    pen = value;
}

QPointF PaintScene::getPreviousPoint() const
{
    return previousPoint;
}

void PaintScene::setPreviousPoint(const QPointF &value)
{
    previousPoint = value;
}

void PaintScene::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    switch (m_typeFigure) {
    case SquareType: {
        Square * item = new Square(event->scenePos(), pen);
        item->setPos(event->pos());
        tempFigure = item;
        break;
    }
    case RombType: {
        Romb * item = new Romb(event->scenePos(), pen);
        item->setPos(event->pos());
        tempFigure = item;
        break;
    }
    case TriangleType: {
        Triangle * item = new Triangle(event->scenePos(), pen);
        item->setPos(event->pos());
        tempFigure = item;
        break;
    }
    case LineType: {
        Line *item = new Line(event->scenePos(), pen);
        item->setPos(event->pos());
        tempFigure = item;
        break;
    }
    case PencilType: {
        Pencil *item = new Pencil(event->scenePos(), pen);
        item->setPos(event->pos());
        tempFigure = item;
        addEllipse(event->scenePos().x() - 5, event->scenePos().y() - 5, 0, 0, QPen(pen), QBrush(Qt::white));

        previousPoint = event->scenePos();
        break;
    }
    case CircleType: {
        Circle *item = new Circle(event->scenePos(), pen);
        item->setPos(event->pos());
        tempFigure = item;
        break;
    }
    default:{
        Line *item = new Line(event->scenePos(), pen);
        item->setPos(event->pos());
        tempFigure = item;
        previousPoint = event->scenePos();
        break;
    }
    }
    this->addItem(tempFigure);
}

void PaintScene::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if(m_typeFigure == PencilType)
    {
        // рисуем
        addLine(previousPoint.x(), previousPoint.y(), event->scenePos().x(), event->scenePos().y(),
                QPen(pen));

        previousPoint = event->scenePos();
    }
    else
    {
        // устанавливаем конечную координату положения мыши в текущуюю, отрисовываем фигуру
        tempFigure->setEndPoint(event->scenePos());
    }

    // обновляем сцену, чтобы не было артефактов
    update(QRectF(0, 0, this->width(), this->height()));
}
