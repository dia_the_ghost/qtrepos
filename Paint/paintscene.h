#ifndef PAINTSCENE_H
#define PAINTSCENE_H

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include "figure.h"


class PaintScene : public QGraphicsScene
{
    Q_OBJECT
                // свойства текущего типа используемой фигуры
    Q_PROPERTY(int typeFigure
               READ typeFigure WRITE setTypeFigure
               NOTIFY typeFigureChanged)
public:
    explicit PaintScene(QObject * parent = 0);
    ~PaintScene();

    int typeFigure() const;
    void setTypeFigure(const int type);

    // перечисление типов
    enum FigureTypes{
        SquareType,
        RombType,
        TriangleType,
        LineType,
        PencilType,
        CircleType
    };
    QPen getPen() const;
    void setPen(const QPen &value);

    QPointF getPreviousPoint() const;
    void setPreviousPoint(const QPointF &value);

signals:
    void typeFigureChanged();
private:
    Figure * tempFigure;
    int m_typeFigure;

    QPen pen;
    QPointF previousPoint;
private:
    void mousePressEvent(QGraphicsSceneMouseEvent * event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent * event);
};

#endif // PAINTSCENE_H
