#include "circle.h"

Circle::Circle(QPointF point, QPen pen, QObject *parent) : Figure(point, pen, parent)
{
    Q_UNUSED(point)
    Q_UNUSED(pen)
}

Circle::~Circle()
{

}

void Circle::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setPen(this->pen());

    QRectF rect(endPoint().x() > startPoint().x() ? startPoint().x() : endPoint().x(),
                endPoint().y() > startPoint().y() ? startPoint().y() : endPoint().y(),
                qAbs(endPoint().x() - startPoint().x()),
                qAbs(endPoint().y() - startPoint().y()));

    painter->drawEllipse(rect);

    Q_UNUSED(widget)
    Q_UNUSED(option)
}
