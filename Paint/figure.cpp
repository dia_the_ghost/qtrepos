#include "figure.h"

Figure::Figure(QPointF point, QPen pen, QObject *parent) : QObject(parent), QGraphicsItem()
{
    // устанавливаем стартовую координату для отрисовки фигуры
    this->setStartPoint(this->mapFromScene(point));
    this->setEndPoint(this->mapFromScene(point));

    this->setPen(pen);

    connect(this, &Figure::pointChanged, this, &Figure::updateRomb);
}

Figure::~Figure()
{

}

QPointF Figure::startPoint() const
{
    return m_startPoint;
}

QPointF Figure::endPoint() const
{
    return m_endPoint;
}

void Figure::setStartPoint(const QPointF point)
{
    m_startPoint = mapFromScene(point);
    emit pointChanged();
}

void Figure::setEndPoint(const QPointF point)
{
    m_endPoint = mapFromScene(point);
    emit pointChanged();
}

QRectF Figure::boundingRect() const
{
    // возвращается область, в которой лежит фигура
    return QRectF((endPoint().x() > startPoint().x() ? startPoint().x() : endPoint().x()) - 5,
                  (endPoint().y() > startPoint().y() ? startPoint().y() : endPoint().y()) - 5,
                  qAbs(endPoint().x() - startPoint().x()) + 10,
                  qAbs(endPoint().y() - startPoint().y()) + 10);
}

QPen Figure::pen() const
{
    return m_pen;
}

void Figure::setPen(const QPen &pen)
{
    m_pen = pen;
}

void Figure::updateRomb()
{
    this->update((endPoint().x() > startPoint().x() ? startPoint().x() : endPoint().x()) - 5,
                 (endPoint().y() > startPoint().y() ? startPoint().y() : endPoint().y()) - 5,
                 qAbs(endPoint().x() - startPoint().x()) + 10,
                 qAbs(endPoint().y() - startPoint().y()) + 10);
}
