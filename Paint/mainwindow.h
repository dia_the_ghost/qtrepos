#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QResizeEvent>
#include <QColorDialog>
#include <QDir>
#include <QFileDialog>
#include <QMenu>
#include <QMenuBar>
#include <QMessageBox>

#include "paintscene.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    int getNum() const;
    void setNum(int value);

private:
    Ui::MainWindow *ui;

    PaintScene * scene;
    QTimer * timer;

    int num;
private:
    void resizeEvent(QResizeEvent * event);
private slots:

    void slotResetPen();

    void slotTimer();
    void on_buttonPolygon_clicked();
    void on_buttonTriangle_clicked();
    void on_buttonSquare_clicked();
    void on_buttoLine_clicked();
    void on_buttonClear_clicked();
    void on_buttonPencil_clicked();
    void on_buttonRed_clicked();
    void on_buttonGreen_clicked();
    void on_buttonBlue_clicked();
    void on_buttonYellow_clicked();
    void on_buttonOrange_clicked();
    void on_buttonLightblue_clicked();
    void on_buttonViolet_clicked();
    void on_buttonBlack_clicked();
    void on_buttonTransparent_clicked();
    void on_spinBox_valueChanged(int arg1);
    void on_buttonSave_clicked();
    void on_buttonmoreColors_clicked();
    void on_buttonOpen_clicked();
    void on_buttonCircle_clicked();
    void on_actionOpen_triggered();
    void on_actionSave_triggered();
    void on_actionAbout_triggered();
    void on_actionAuthors_triggered();
};

#endif // MAINWINDOW_H
