#include "square.h"

Square::Square(QPointF point, QPen pen, QObject *parent) : Figure(point, pen, parent)
{
    Q_UNUSED(point)
    Q_UNUSED(pen)
}

Square::~Square()
{

}

void Square::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    // ЗАМЕНИТЬ
    painter->setPen(this->pen());

    QRectF rect(endPoint().x() > startPoint().x() ? startPoint().x() : endPoint().x(),
                endPoint().y() > startPoint().y() ? startPoint().y() : endPoint().y(),
                qAbs(endPoint().x() - startPoint().x()),
                qAbs(endPoint().y() - startPoint().y()));

    painter->drawRect(rect);

    Q_UNUSED(widget)
    Q_UNUSED(option)

}
