#-------------------------------------------------
#
# Project created by QtCreator 2017-07-01T07:58:39
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = dynamicButtons
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    mbutton.cpp \
    customshadoweffect.cpp \
    clock.cpp \
    mediator.cpp

HEADERS  += widget.h \
    mbutton.h \
    customshadoweffect.h \
    clock.h \
    mediator.h

CONFIG += mobility
MOBILITY = 

RESOURCES += \
    res/res.qrc

DISTFILES += \
    android/AndroidManifest.xml \
    android/res/values/libs.xml \
    android/build.gradle

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

