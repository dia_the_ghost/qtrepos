#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_addButton_clicked()
{
    QDynamicButton * button = new QDynamicButton(this);
    button->setText("я ебал твою мать " + QString::number(button->getID()) + " раз");

    ui->verticalLayout->addWidget(button);

    connect(button, SIGNAL(clicked()), this, SLOT(slotGetNumber()));
}

void MainWindow::on_deleteButton_clicked()
{
    for(int i = 0; i < ui->verticalLayout->count(); i++)
    {
        QDynamicButton * button = qobject_cast<QDynamicButton*> (ui->verticalLayout->itemAt(i)->widget());

        if(button->getID() == ui->lineEdit->text().toInt())
        {
            button->hide();
            delete button;
        }
    }
}

void MainWindow::slotGetNumber()
{
    QDynamicButton * button = (QDynamicButton*)sender();

    ui->lineEdit->setText(QString::number(button->getID()));
}
