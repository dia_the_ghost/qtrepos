//#include "widget.h"
//#include <QApplication>

//int main(int argc, char *argv[])
//{
//    QApplication a(argc, argv);
//    Widget w;
//    w.show();

//    return a.exec();
//}

#include "mediator.h"
#include "widget.h"
#include <QApplication>
#include <QMainWindow>
#include <QAction>
#include <QMenuBar>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QMainWindow mainWindow; // окно программы
    Mediator mediator; // игровое поле
    QMenuBar *menuBar; // меню

    mainWindow.setCentralWidget(&mediator);
    mainWindow.setWindowIcon(QIcon(":/mine.png"));
    mainWindow.setWindowTitle("Минер");
    menuBar = mainWindow.menuBar();

    QAction *newAct = new QAction(QObject::tr("&New game"), &mainWindow);
    QObject::connect(newAct, SIGNAL(triggered()), &mediator, SLOT(on_gameReset()));
    menuBar->addAction(newAct);

    QAction *authAct = new QAction(QObject::tr("&About"), &mainWindow);
    QObject::connect(authAct, SIGNAL(triggered()), &mediator, SLOT(on_about()));
    menuBar->addAction(authAct);



    mainWindow.show();

    return a.exec();
}
