#ifndef QCOORD_H
#define QCOORD_H


class QCoord
{
public:
    QCoord();

private:
    int x;
    int y;

public:
    int getX();
    int getY();

    void setX(int _x);
    void setY(int _y);
};

#endif // QCOORD_H
