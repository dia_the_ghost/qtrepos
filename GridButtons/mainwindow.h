#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QGridLayout>
#include <QList>
#include "qbutton.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_addButton_clicked();
    void on_deleteButton_clicked();
    void slotGetNumber();

    void on_deleteAll_clicked();

    void refresh();

private:
    Ui::MainWindow *ui;

    int A;
    int B;

    QGridLayout * grid;

};

#endif // MAINWINDOW_H
