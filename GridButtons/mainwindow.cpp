#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_addButton_clicked()
{
    A = ui->lineI->text().toInt();
    B = ui->lineJ->text().toInt();
    if(A > 10) A = 10;
    if(B > 10) B = 10;

    // удаляем старый лаяут если он существует и создаем новый
    refresh();

    for(int i = 0; i < A; i++)
    {
        for(int j = 0; j < B; j++)
        {
            QButton *button = new QButton(this);
            button->setText(QString::number(i) + " " + QString::number(j) + "/" + QString::number(button->getID()));
            grid->addWidget(button, i, j);

            connect(button, SIGNAL(clicked()), this, SLOT(slotGetNumber()));
        }
    }
}

void MainWindow::on_deleteButton_clicked()
{
    if(!grid) return;

    qDebug() << grid->count();

    for(int i = 0; i < grid->rowCount(); i++)
    {
        for(int j = 0; j < grid->columnCount(); j++)
        {
            QButton *button = qobject_cast<QButton*>(grid->itemAtPosition(i, j)->widget());

            if(button->getID() == ui->lineEdit->text().toInt())
            {
                button->hide();
                //delete button;
            }
        }
    }

    ui->lineEdit->clear();
}

void MainWindow::slotGetNumber()
{
    QButton *button = (QButton*) sender();

    ui->lineEdit->setText(QString::number(button->getID()));
}

void MainWindow::on_deleteAll_clicked()
{
    for(int i = 0; i < grid->rowCount(); i++)
    {
        for(int j = 0; j < grid->columnCount(); j++)
        {
            QButton *button = qobject_cast<QButton*>(grid->itemAtPosition(i, j)->widget());
            delete button;
        }
    }
}

void MainWindow::refresh()
{
    grid = new QGridLayout();
    ui->gridLayout->addLayout(grid, 1, 1);
}
