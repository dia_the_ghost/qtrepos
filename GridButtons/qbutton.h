#ifndef QBUTTON_H
#define QBUTTON_H


#include <QPushButton>

class QButton : public QPushButton
{
    Q_OBJECT
public:
    explicit QButton(QWidget *parent = 0);
    ~QButton();

    static int ResID;   // Статическая переменная, счетчик номеров кнопок
    int getID();

private:
    int buttonID = 0;
};

#endif // QBUTTON_H
