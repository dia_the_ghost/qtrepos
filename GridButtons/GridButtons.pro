#-------------------------------------------------
#
# Project created by QtCreator 2016-12-25T01:00:37
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GridButtons
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    qbutton.cpp

HEADERS  += mainwindow.h \
    qbutton.h

FORMS    += mainwindow.ui
