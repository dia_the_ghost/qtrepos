import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQml 2.2

import ClockCircle 1.0

Window {
    visible: true
    width: 400
    height: 400
    title: qsTr("Timer")

    ClockCircle {
        id: clockCircle

        anchors.top: parent.top
        anchors.topMargin: 50
        anchors.horizontalCenter: parent.horizontalCenter
        width: 200
        height: 200

        name: "clock"
        backgroundColor: "whiteSmoke"
        borderActiveColor: "LightSlateGray"
        borderNonActiveColor: "LightSteelBlue"

        Text {
            id: textTimer
            anchors.centerIn: parent
            font.bold: true
            font.pixelSize: 24
        }

        onCircleTimeChanged: {
            textTimer.text = Qt.formatTime(circleTime, "mm: ss. zzz")
        }
    }

    Button {
        id: start
        text: "Start"

        anchors.left: parent.left
        anchors.leftMargin: 20
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20

        onClicked: clockCircle.start();
    }

    Button {
        id: stop
        text: "Stop"

        anchors {
            horizontalCenter: parent.horizontalCenter
            bottom: parent.bottom
            bottomMargin: 20
        }

        onClicked: clockCircle.stop();
    }

    Button {
        id: clear
        text: "Clear"

        anchors {
            right: parent.right
            rightMargin: 20
            bottom: parent.bottom
            bottomMargin: 20
        }

        onClicked: clockCircle.clear();
    }


}
