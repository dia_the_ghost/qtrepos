#include "clockcircle.h"


ClockCircle::ClockCircle(QQuickItem *parent) : QQuickPaintedItem(parent), m_backgroundColor(Qt::white),
    m_borderActiveColor(Qt::blue), m_borderNonActiveColor(Qt::gray), m_angle(0), m_circleTime(QTime(0,0,0,0))
{
    internalTimer = new QTimer(this);

    connect(internalTimer, &QTimer::timeout, [=](){
       setAngle(angle() - 0.3);
       setCircleTime(circleTime().addMSecs(50));
       update();
    });
}

void ClockCircle::paint(QPainter *painter)
{
    QBrush brush(m_backgroundColor);
    QBrush brushActive(m_borderActiveColor);
    QBrush brushNonActive(m_borderNonActiveColor);

    painter->setPen(Qt::NoPen);
    painter->setRenderHints(QPainter::Antialiasing, true);

    painter->setBrush(brushNonActive);
    painter->drawEllipse(boundingRect().adjusted(1, 1, -1, -1));

    painter->setBrush(brushActive);
    painter->drawPie(boundingRect().adjusted(1, 1, -1, -1), 90*16, m_angle*16);

    painter->setBrush(brush);
    painter->drawEllipse(boundingRect().adjusted(10, 10, -10, -10));
}

void ClockCircle::clear()
{
    setCircleTime(QTime(0, 0, 0, 0));
    setAngle(0);
    update();
    emit cleared();
}

void ClockCircle::start()
{
    internalTimer->start(50);
}

void ClockCircle::stop()
{
    internalTimer->stop();
}

QString ClockCircle::name() const
{
    return m_name;
}

QColor ClockCircle::backgroundColor() const
{
    return m_backgroundColor;
}

QColor ClockCircle::borderActiveColor() const
{
    return m_borderActiveColor;
}

QColor ClockCircle::borderNonActiveColor() const
{
    return m_borderNonActiveColor;
}

qreal ClockCircle::angle() const
{
    return m_angle;
}

QTime ClockCircle::circleTime() const
{
    return m_circleTime;
}

void ClockCircle::setName(const QString name)
{
    if(m_name == name) return;

    m_name = name;
    emit nameChanged(name);
}

void ClockCircle::setBackgroundColor(const QColor backgroundColor)
{
    if(m_backgroundColor == backgroundColor) return;

    m_backgroundColor = backgroundColor;
    emit backgroundColorChanged(backgroundColor);
}

void ClockCircle::setBorderActiveColor(const QColor borderActiveColor)
{
    if(m_borderActiveColor == borderActiveColor) return;

    m_borderActiveColor = borderActiveColor;
    emit borderActiveColorChanged(borderActiveColor);
}

void ClockCircle::setBorderNonActiveColor(const QColor borderNonActiveColor)
{
    if(m_borderNonActiveColor == borderNonActiveColor) return;

    m_borderNonActiveColor = borderNonActiveColor;
    emit borderNonActiveColorChanged(borderNonActiveColor);
}

void ClockCircle::setAngle(const qreal angle)
{
    if(m_angle == angle) return;

    m_angle = angle;

    if(m_angle <= -360) m_angle = 0;
    emit angleChanged(m_angle);
}

void ClockCircle::setCircleTime(const QTime circleTime)
{
    if(m_circleTime == circleTime) return;

    m_circleTime = circleTime;
    emit circleTimeChanged(circleTime);
}
