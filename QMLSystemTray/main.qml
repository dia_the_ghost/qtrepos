import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Window 2.0
import QSystemTrayIcon 1.0

ApplicationWindow {
    id: application
    visible: true
    width: 640
    height: 480
    title: qsTr("SystemTray")

    QSystemTrayIcon{
        id: systemTray

                                    // инициализация трея
        Component.onCompleted: {
            icon = iconTray         // иконка
            toolTip = "Tray program"
            show();
        }

        onActivated: {
            if(reason === 1){
                trayMenu.popup()
            }
            else{
                if(application.visibility === Window.Hidden){
                    application.show()
                }
                else {
                    application.hide()
                }
            }
        }
    }

    // меню системного трея
    Menu {
        id: trayMenu

        MenuItem {
            text: qsTr("Развернуть окно")
            onTriggered: application.show()
        }

        MenuItem {
            text: qsTr("Выход")
            onTriggered: {
                systemTray.hide()
                Qt.quit()
            }
        }
    }



    CheckBox {
        id: checkTray
        anchors.centerIn: parent
        text: qsTr("Сворачивать в трей")
    }

    Button {
        text: qsTr("Выход")
        anchors.top: parent.top
        anchors.left: parent.left
        width: parent.width - 1
        height: (parent.height / 2) - 100

        onClicked: close()
    }

    // обработка события закрытия окна
    onClosing: {
        if(checkTray.checked === true){
            close.accepted = false
            application.hide()
        }
        else{
            Qt.quit()
        }
    }
}
