import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1

ApplicationWindow {
    id: mainWindow
    visible: true
    width: 640
    height: 480
    title: qsTr("Переключение между окнами")

   Rectangle {
       anchors.fill: parent
       color: "white"

       GridLayout {
           id: grid
           anchors.fill: parent

           rows: 2
           columns: 1

           Rectangle {
               Layout.fillHeight: true
               Layout.fillWidth: true
               Layout.column: 1
               Layout.row: 1

               Button {
                   text: qsTr("Первое окно")
                   anchors.centerIn: parent
                   width: 300
                   height: 50

                   onClicked: {
                       firstWindow.show()
                       mainWindow.hide()
                   }
               }
           }

           Rectangle {
               Layout.fillHeight: true
               Layout.fillWidth: true
               Layout.column: 1
               Layout.row: 2

               Button {
                   text: qsTr("Второе окно")
                   anchors.centerIn: parent
                   width: 300
                   height: 50

                   onClicked: {
                       secondWindow.show()
                       mainWindow.hide()
                   }
               }
           }
       }
   }
   AnotherWindow {
       id: firstWindow
       title: qsTr("Первое окно")

       onSignalExit: {
           firstWindow.close()
           mainWindow.show()
       }
   }
   AnotherWindow {
       id: secondWindow
       title: qsTr("Второе окно")

       onSignalExit: {
           secondWindow.close()
           mainWindow.show()
       }
   }
}
