/*#ifndef MYCLASS_H
#define MYCLASS_H

#include <QObject>

class MyClass : public QObject
{
    Q_OBJECT
public:
    Q_INVOKABLE QObject* createObject();
    Q_INVOKABLE void useObject();

private:
    QObject * m_object();
};

#endif // MYCLASS_H
*/
#ifndef MYCLASS_H
#define MYCLASS_H

#include <QObject>

class MyClass : public QObject
{
    Q_OBJECT

public:
    Q_INVOKABLE QObject* createObject();    // Создание объекта для испытаний
    Q_INVOKABLE void useObject();           // Использование испытуемого объекта

private:
    QObject* m_object;      // Подопытный кролик
};

#endif // MYCLASS_H
