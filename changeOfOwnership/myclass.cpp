#include "myclass.h"
#include <QString>
#include <QDebug>
//#include <QObject>


QObject *MyClass::createObject()
{
    //m_object = new QObject(this);
    m_object = new QObject;

    return m_object;
}

void MyClass::useObject()
{
    qDebug() << m_object->objectName();
}
