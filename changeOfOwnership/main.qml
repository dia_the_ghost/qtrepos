import QtQuick 2.5
import QtQuick.Controls 1.3

ApplicationWindow {
    visible: true
    width: 640
    height: 480

    property QtObject objectFromCppWorld: null

    Column {
        anchors.centerIn: parent

        Button {
            text: "create object in cpp and pass it to QML"
            onClicked: objectFromCppWorld = myClass.createObject()
        }

        Button {
            text: "null the reference in QML"
            onClicked: {
                objectFromCppWorld = null
                gc()
            }
        }

        Button {
            text: "use created object in cpp"
            onClicked: myClass.useObject()
        }
    }
}
