#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);


    trayIcon = new QSystemTrayIcon(this);
    trayIcon->setIcon(this->style()->standardIcon(QStyle::SP_ComputerIcon));
    trayIcon->show();

    connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
            this, SLOT(iconActivated(QSystemTrayIcon::ActivationReason)));


     // Регистрируем HotKey "ALT+SHIFT+D"

    RegisterHotKey((HWND)MainWindow::winId(), 100, (MOD_SHIFT), 'D');

}

MainWindow::~MainWindow()
{
    delete ui;
}

bool MainWindow::nativeEvent(const QByteArray &eventType, void *message, long *result)
{
    Q_UNUSED(eventType)
    Q_UNUSED(result)

    // Преобразуем указатель message в MSG WinAPI

    MSG * msg = reinterpret_cast<MSG*>(message);

    // Если сообщение является HotKey, то ...
    if(msg->message == WM_HOTKEY)
    {
        // ... проверяем идентификатор HotKey
        if(msg->wParam == 100)
        {
             // Сообщаем об этом в консоль
            qDebug() << "HotKey worked";
            return true;
        }
    }

    return false;

}

void MainWindow::iconActivated(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason) {
    case QSystemTrayIcon::Trigger:
        !isVisible() ? show() : hide();
        break;

    default:
        break;
    }
}
