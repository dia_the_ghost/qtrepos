import QtQuick 2.7
import QtQuick.Controls 1.4

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("QML Tab View")

    Button{
        id: button
        text: qsTr("Кнопка 1")
        anchors.top: parent.top
        anchors.topMargin: 5
        anchors.left: parent.left
        anchors.leftMargin: 5

        onClicked: {
            tabView.getTab(0).item.children[0].text = "Кнопка 1"
        }
    }

    TabView {
        id: tabView
        anchors.top: button.bottom
        anchors.topMargin: 5
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom

        Tab {
            id: tab1
            title: qsTr("Первая вкладка")

            component: Item {
                id: rect1

                Button {
                    id: buttonTab
                    text: qsTr("Кнопка 2")

                    anchors.top: parent.top
                    anchors.topMargin: 5
                    anchors.left: parent.left
                    anchors.leftMargin: 5

                    onClicked: {
                        button.text = "Кнопка 2"
                    }
                }
            }
        }

        Tab {
            id: tab2
            title: qsTr("Вторая вкладка")
        }
    }


}
