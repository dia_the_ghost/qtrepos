#include <QStyle>
#include "qcalc.h"

#include <QDebug>

QCalc::QCalc(QWidget *parent)
    : QWidget(parent)
{
    setWindowTitle("Калькулятор");

    sum = 0.0;
    factor = 0.0;
    waitingForOperand = true;

    display = new QLineEdit("0");
    display->setReadOnly(true);
    display->setAlignment(Qt::AlignRight);
    display->setMaxLength(15);

    QFont font("Times New Roman");
    font.setPointSize(16);
    display->setFont(font);


    //создадим 10 кнопок с цифрами
    for(int i(0); i < NumDigitButtons; ++i)
    {
        digitButtons[i] = createButton(QString::number(i), SLOT(digitClicked()));
    }


    QGridLayout * mainLayout = new QGridLayout;
    mainLayout->addWidget(display, 0, 0, 1, 6);

    // припихнем 9 кнопок с цифрами от 1 до 9
    for(int i(1); i < NumDigitButtons; ++i)
    {
        // по непонятной магической формуле вычислим столбцы и строки
        int row = ((9 - i) / 3) + 2;
        int column = ((i - 1) % 3) + 1;
        mainLayout->addWidget(digitButtons[i], row, column);
    }
    // припихнем кнопку 0
    mainLayout->addWidget(digitButtons[0], 5, 1);


    /// кнопки + - * /

    plusButton = createButton(tr("+"), SLOT(additiveOperatorClicked()));
    minusButton = createButton(tr("-"), SLOT(additiveOperatorClicked()));
    timesButton = createButton(tr("*"), SLOT(multiplicativeOperatorClicked()));
    divineButton = createButton(tr("/"), SLOT(multiplicativeOperatorClicked()));

    /// кнопки С =

    Button * clearAll = createButton(tr("C"), SLOT(clearAll()));
    Button * equalButton = createButton(tr("="), SLOT(equalClicked()));

    //добавим остальные кнопки на виджет
    mainLayout->addWidget(clearAll, 5, 2);
    mainLayout->addWidget(equalButton, 5, 3);

    mainLayout->addWidget(plusButton, 2, 4);
    mainLayout->addWidget(minusButton, 3, 4);
    mainLayout->addWidget(timesButton, 4, 4);
    mainLayout->addWidget(divineButton, 5, 4);

    setLayout(mainLayout);
}

QCalc::~QCalc()
{

}

void QCalc::digitClicked()
{
    /// кто-то нажал цифру и мы оказались тут
    /// что поделать

    Button * clickedButton = qobject_cast<Button *>(sender());
    int val = clickedButton->text().toInt();

    if(display->text() == "0" && val == 0.0) return;

    // мы больше не ждем операнда, потому что его ввели
    if(waitingForOperand)
    {
        display->clear();
        waitingForOperand = false;
    }

    display->setText(display->text() + QString::number(val));
}

void QCalc::clearAll()
{
    sum = 0.0;
    factor = 0.0;
    additiveOperator.clear();
    multiplicativeOperator.clear();

    display->setText("0");
    waitingForOperand = true;
}

void QCalc::multiplicativeOperatorClicked()
{
    /// кто то нажал * или /

    Button * clickedButton = qobject_cast<Button *>(sender());
    QString clickedOperator = clickedButton->text();
    double operand = display->text().toDouble();

    if(!multiplicativeOperator.isEmpty())
    {
        if(!calculate(operand, multiplicativeOperator))
        {
            abortOperation();
            return;
        }
        display->setText(QString::number(factor));
    }
    else
    {
        factor = operand;
    }

    multiplicativeOperator = clickedOperator;
    waitingForOperand = true;
}

void QCalc::additiveOperatorClicked()
{
    /// кто-то нажал + или -

    Button * clickedButton = qobject_cast<Button *>(sender());
    QString clickedOperator = clickedButton->text();
    double operand = display->text().toDouble();

    if(!additiveOperator.isEmpty())
    {
        if(!calculate(operand, additiveOperator))
        {
            abortOperation();
            return;
        }
        display->setText(QString::number(sum));
    }
    else
    {
        sum = operand;
    }

    additiveOperator = clickedOperator;
    waitingForOperand = true;
}

void QCalc::equalClicked()
{
    double operand = display->text().toDouble();

    /// * /
    if(!multiplicativeOperator.isEmpty())
    {
        if(!calculate(operand, multiplicativeOperator))
        {
            abortOperation();
            return;
        }
        operand = factor;
        factor = 0.0;
        multiplicativeOperator.clear();
    }

    /// + -
    if(!additiveOperator.isEmpty())
    {
        if(!calculate(operand, additiveOperator))
        {
            abortOperation();
            return;
        }
        additiveOperator.clear();
    }

    ///
    else
    {
        sum = operand;
    }

    display->setText(QString::number(sum));
    sum = 0.0;
    waitingForOperand = true;
}

Button *QCalc::createButton(const QString &text, const char *member)
{
    Button * btn = new Button(text);
    connect(btn, SIGNAL(clicked()), this, member);
    return btn;
}

void QCalc::abortOperation()
{
    clearAll();
    display->setText(tr("ERROR"));
}

bool QCalc::calculate(double rightOperand, const QString &pendingOperator)
{
    /// складываем
    if (pendingOperator == tr("+"))
    {
        sum += rightOperand;
    }
    /// вычитаем
    else if (pendingOperator == tr("-"))
    {
       sum -= rightOperand;
    }
    /// умножаем
    else if (pendingOperator == tr("*"))
    {
        factor *= rightOperand;
    }
    ///
    else if (pendingOperator == tr("/"))
    {
        if(rightOperand == 0.0) return false;
        factor /= rightOperand;
    }

    return true;
}

void QCalc::keyPressEvent(QKeyEvent *e)
{
    if ((e->key() >= Qt::Key_0) && (e->key() <= Qt::Key_9))
    {
        digitButtons[e->text().toInt()]->clicked();
    }


    switch (e->key()) {
    case Qt::Key_Equal:
        this->equalClicked();
        break;

    case Qt::Key_Return:
        this->equalClicked();
        break;

    case Qt::Key_Minus:
        minusButton->clicked();
        break;

    case Qt::Key_Plus:
        plusButton->clicked();
        break;

    case '*':
        timesButton->clicked();
        break;

    case '/':
        divineButton->clicked();
        break;

    case Qt::Key_C:
        clearAll();
        break;

    default:
        break;
    }
}
