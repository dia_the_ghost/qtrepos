#ifndef BUTTON_H
#define BUTTON_H

#include <QObject>
#include <QToolButton>
#include "customshadoweffect.h"

class Button : public QToolButton
{
    Q_OBJECT
public:
    explicit Button(const QString &text, QWidget *parent = 0);

    QSize sizeHint() const Q_DECL_OVERRIDE;

protected:
    void setStyles();
};

#endif // BUTTON_H
