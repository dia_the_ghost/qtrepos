#ifndef QCALC_H
#define QCALC_H

#include <QWidget>
#include <QLineEdit>
#include <QGridLayout>
#include <QKeyEvent>
#include <QEvent>
#include "button.h"

class QCalc : public QWidget
{
    Q_OBJECT

public:
    QCalc(QWidget *parent = 0);
    ~QCalc();

private slots:
    void digitClicked();
    void clearAll();

    void multiplicativeOperatorClicked();
    void additiveOperatorClicked();

    void equalClicked();

private:
    Button *createButton(const QString &text, const char * member);
    void abortOperation();
    bool calculate(double rightOperand, const QString &pendingOperator);

    void keyPressEvent(QKeyEvent *e);


    QString additiveOperator;
    QString multiplicativeOperator;
    bool waitingForOperand;

    double sum;
    double factor;

    QLineEdit * display;
    enum { NumDigitButtons = 10 };
    Button * digitButtons[NumDigitButtons];

    //
    Button * minusButton;
    Button * plusButton;
    Button * timesButton;
    Button * divineButton;



};

#endif // QCALC_H
