#-------------------------------------------------
#
# Project created by QtCreator 2017-02-21T05:48:52
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QCalc
TEMPLATE = app


SOURCES += main.cpp\
        qcalc.cpp \
    button.cpp \
    customshadoweffect.cpp

HEADERS  += qcalc.h \
    button.h \
    customshadoweffect.h
