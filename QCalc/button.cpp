#include "button.h"

Button::Button(const QString &text, QWidget *parent) : QToolButton(parent)
{
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    setText(text);
    setStyles();
}

QSize Button::sizeHint() const
{
    QSize size = QToolButton::sizeHint();
    size.rheight() += 20;
    size.rwidth() = qMax(size.width(), size.height());
    return size;
}

void Button::setStyles()
{
    setFont(QFont ("Times New Roman", 12));
    setStyleSheet("* { background-color: rgba(45, 20, 50, 20); border-radius: 2;}");

   // Тень
    CustomShadowEffect * bodyShadow = new CustomShadowEffect();
    bodyShadow->setBlurRadius(20.0);
    bodyShadow->setDistance(4.0);
    bodyShadow->setColor(QColor(30, 0, 100, 80));
    setAutoFillBackground(true);
    setGraphicsEffect(bodyShadow);
}
